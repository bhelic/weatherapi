package com.hackerrank.weather.controller;

import com.hackerrank.weather.service.weather.dto.*;
import com.hackerrank.weather.service.weather.WeatherService;
import io.swagger.v3.oas.annotations.Operation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * Weather RestController.
 * Date 23/07/2021.
 *
 * @autor: Nicolás Ayza.
 */
@RestController
@RequestMapping("/weather")
public class WeatherApiRestController {

    @Autowired
    WeatherService weatherService;

    @PostMapping
    @Operation(summary = "Save weather.")
    public ResponseEntity<WeatherResponse> save(@RequestBody WeatherConsume consume) {
        return new ResponseEntity<>(weatherService.save(consume), HttpStatus.CREATED);
    }

    @GetMapping("/{id}")
    @Operation(summary = "Find weather by Id.")
    public ResponseEntity<WeatherResponse> getById(@PathVariable("id") Integer id) {
        return ResponseEntity.ok().body(weatherService.findById(id));
    }

    @GetMapping
    @Operation(summary = "Find weathers with filters and sort.")
    public ResponseEntity<List<WeatherResponse>> getAllByFilterAndSort(
            @RequestParam(name = "city", defaultValue = "", required = false) String cityFilter,
            @RequestParam(name = "date", defaultValue = "", required = false) String dateFilter,
            @RequestParam(name = "sort", defaultValue = "", required = false) String dateSort
    ) {
        return ResponseEntity.ok().body(weatherService.findAllByFilterAndSort(cityFilter, dateFilter, dateSort));
    }

}
