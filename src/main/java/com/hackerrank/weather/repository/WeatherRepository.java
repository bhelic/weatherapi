package com.hackerrank.weather.repository;

import com.hackerrank.weather.model.Weather;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

@Repository
public interface WeatherRepository extends JpaRepository<Weather, Integer>, JpaSpecificationExecutor<Weather> {

    //List<Weather> findByCityContainingIgnoreCase(String city, Sort sort);
    //List<Weather> findByDate(Date date, Sort sort);
    //List<Weather> findByCityContainingIgnoreCaseAndDate(String city, Date date, Sort sort);

}
