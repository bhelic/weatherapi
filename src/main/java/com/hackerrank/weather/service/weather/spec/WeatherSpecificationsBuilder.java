package com.hackerrank.weather.service.weather.spec;

import com.hackerrank.weather.config.SearchCriteria;
import com.hackerrank.weather.model.Weather;
import org.springframework.data.jpa.domain.Specification;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class WeatherSpecificationsBuilder {

    private final List<SearchCriteria> params;

    public WeatherSpecificationsBuilder() {
        params = new ArrayList<>();
    }

    public WeatherSpecificationsBuilder with(String key, String operation, Object value) {
        params.add(new SearchCriteria(key, operation, value));
        return this;
    }

    public Specification<Weather> build() {
        if (params.size() == 0) {
            return null;
        }

        List<Specification> specs = params.stream()
                .map(WeatherSpecification::new)
                .collect(Collectors.toList());

        Specification result = specs.get(0);

        for (int i = 1; i < params.size(); i++) {
            result = Specification.where(result).and(specs.get(i));
        }
        return result;
    }

}
