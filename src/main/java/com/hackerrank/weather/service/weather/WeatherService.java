package com.hackerrank.weather.service.weather;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.hackerrank.weather.model.Weather;
import com.hackerrank.weather.service.weather.spec.WeatherSpecificationsBuilder;
import com.hackerrank.weather.service.weather.dto.*;
import com.hackerrank.weather.repository.WeatherRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.rest.webmvc.ResourceNotFoundException;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * Weather Service.
 * Date 23/07/2021.
 *
 * @autor: Nicolás Ayza.
 */
@Service
public class WeatherService {

    @Autowired
    WeatherRepository weatherRepository;

    @Autowired
    private ObjectMapper objectMapper;

    SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");

    public WeatherResponse save(WeatherConsume consume) {
        Weather weather = this.objectMapper.convertValue(consume, Weather.class);
        return this.objectMapper.convertValue(weatherRepository.save(weather), WeatherResponse.class);
    }

    public WeatherResponse findById(int id) {
        Optional<Weather> opWeather = weatherRepository.findById(id);
        if (opWeather.isPresent()) {
            return this.objectMapper.convertValue(opWeather.get(), WeatherResponse.class);
        } else {
            throw new ResourceNotFoundException("WeatherEntity not found!");
        }
    }

    public List<WeatherResponse> findAllByFilterAndSort(String cityFilter, String dateFilter, String dateSort) {
        WeatherSpecificationsBuilder specificationsBuilder = new WeatherSpecificationsBuilder();
        Sort sortCriteria;

        //Se establece el criterio de ordenamiento según dateSort. Por defecto se ordena por Id ASC.
        if (dateSort.equals("date")) {
            sortCriteria = Sort.by("date").ascending();
        } else if (dateSort.equals("-date")) {
            sortCriteria = Sort.by("date").descending();
        } else {
            sortCriteria = Sort.by("id").ascending();
        }

        //Por cada filtro presente (cityFilter, dateFilter) agrego un criterio de búsqueda.
        if (!cityFilter.isEmpty()) {
            specificationsBuilder.with("city", "=", cityFilter);
        }
        if (!dateFilter.isEmpty()) {
            try {
                specificationsBuilder.with("date", "=", simpleDateFormat.parse(dateFilter));
            } catch (ParseException e) {
                throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Error parsing date string. Date format must be yyyy-MM-dd.");
            }
        }
        return this.objectMapper.convertValue(
                weatherRepository.findAll(specificationsBuilder.build(), sortCriteria),
                new TypeReference<List<WeatherResponse>>() {
                });
    }

}
































